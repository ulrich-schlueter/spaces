# 🔥 Features

- **Draggable** refers to the ability to drag and move targets.
- **Resizable** indicates whether the target's width and height can be increased or decreased.
- **Scalable** indicates whether the target's x and y can be scale of transform.
- **Rotatable** indicates whether the target can be rotated.
- **Warpable** indicates whether the target can be warped (distorted, bented).
- **Pinchable** indicates whether the target can be pinched with draggable, resizable, scalable, rotatable.
- **Groupable** indicates Whether the targets can be moved in group with draggable, resizable, scalable, rotatable.
- **Snappable** indicates whether to snap to the guideline.
- **OriginDraggable\*** indicates Whether to drag origin.
- **Clippable** indicates Whether to clip the target.
- **Roundable** indicates Whether to show and drag or double click border-radius.
- Support SVG Elements (svg, path, line, ellipse, g, rect, ...etc)
- Support Major Browsers
- Support 3d Transform

---

## ⚙️ Installation

```sh
$ npm i vue-moveable
```

## 📄 Documents

- [**Moveable Handbook**](https://github.com/daybrush/moveable/blob/master/handbook/handbook.md)
- [**How to use Group**](https://github.com/daybrush/moveable/blob/master/handbook/handbook.md#toc-group)
- [**How to use custom CSS**](https://github.com/daybrush/moveable/blob/master/handbook/handbook.md#toc-custom-css)
- [API Documentation](https://daybrush.com/moveable/release/latest/doc/)

## 🚀 How to use

```vue
sdf
```
