package main

import (
	"net/http"

	"github.com/foolin/goview"
	"github.com/foolin/goview/supports/echoview-v4"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	log "github.com/sirupsen/logrus"

	"j7labs.de/spaces/pkg/config"
	"j7labs.de/spaces/pkg/spaces"
)

type App struct {
	config config.Config
	sm     *spaces.Manager
	m      *spaces.Markdowner
}

func (a *App) init() {
	config := config.Config{
		ButtonClasses:  "btn btn-outline-primary",
		MarkdownFolder: "spaces/markdowns",
		SpacesFolder:   "spaces",
		Htmlfolder:     "html",
		ViewFolder:     "html/views",
	}
	a.config = config
	a.sm = spaces.NewManager("Spacessssss !!!", config.SpacesFolder)
	a.m = &spaces.Markdowner{
		ButtonClasses:  config.ButtonClasses,
		MarkdownFolder: config.MarkdownFolder,
	}
}

func (a *App) IndexRenderMap(pageName string) *echo.Map {

	e := &echo.Map{
		"pageName":    pageName,
		"data":        a.sm.Pages[pageName],
		"spaces":      a.sm.SortedPageNames(),
		"genMarkdown": a.m.GenMarkdown,
	}

	return e
}

func (a *App) SinglePageRenderMap(pageName string, space *spaces.Space) *echo.Map {
	e := &echo.Map{
		"page":        pageName,
		"space":       space,
		"spaces":      a.sm.SortedPageNames(),
		"genMarkdown": a.m.GenMarkdown,
	}
	return e

}

var app = App{}

func main() {

	app.init()

	e := echo.New()
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	//e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
	//	AllowOrigins: []string{"*"},
	//	AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	//}))
	e.Use(middleware.CORS())

	e.Renderer = echoview.New(goview.Config{
		Root: "html/views", //template root path)
	})

	e.Static("/static", "html/static")
	e.Static("/js", "html/static/js")
	e.Renderer = echoview.Default()

	app.sm.InitPages()

	e.GET("/", func(c echo.Context) error {
		pageName := app.sm.SortedPageNames()[0]
		return c.Render(http.StatusOK, "index", app.IndexRenderMap(pageName))
	})

	e.GET("/details", func(c echo.Context) error {
		ID := c.QueryParam("ID")
		pageName := c.QueryParam("page")
		space, err := app.sm.SpaceByID(pageName, ID)
		if err == nil {
			log.WithFields(log.Fields{
				"omg":    true,
				"number": 122,
			}).Info("single!")

			return c.Render(http.StatusOK, "single", app.SinglePageRenderMap(pageName, space))
		}

		log.WithFields(log.Fields{
			"omg":    true,
			"number": 122,
		}).Info("Index!")

		return c.Render(http.StatusOK, "index", app.IndexRenderMap(pageName))

	})

	e.GET("/loadPage", func(c echo.Context) error {
		//pageName := app.sm.SortedPageNames()[0]
		pageName := c.QueryParam("page")

		log.WithFields(log.Fields{
			"omg":    true,
			"number": 122,
		}).Info("Index!")

		return c.Render(http.StatusOK, "index", app.IndexRenderMap(pageName))
	})

	e.GET("/test", func(c echo.Context) error {

		pageName := c.QueryParam("page")
		return c.Render(http.StatusOK, "test.html", app.IndexRenderMap(pageName))

	})

	e.Logger.Fatal(e.Start(":1323"))
}
