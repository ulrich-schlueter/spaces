FROM registry.access.redhat.com/ubi9/go-toolset as build

USER root

ENV GOPATH=$APP_ROOT
ENV GOBIN=$APP_ROOT/bin
RUN echo $APP_ROOT
WORKDIR $GOPATH/src/spaces
COPY . $GOPATH/src/spaces/

RUN file="$(ls -1 $GOPATH/src/spaces/)" && echo $file
RUN pwd
RUN ls -al

RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main ./cmd
RUN ls -al
RUN ls -al bin
RUN ls -al cmd

USER 1001


FROM registry.access.redhat.com/ubi9/ubi-micro
RUN ls -al
RUN pwd
COPY --from=build /opt/app-root/src/spaces/main spacer
RUN mkdir spaces
COPY --from=build /opt/app-root/src/spaces/spaces spaces
RUN mkdir html
COPY --from=build /opt/app-root/src/spaces/html html
COPY --from=build /opt/app-root/src/spaces/starter.sh .

RUN ls -al
RUN ls -al html
CMD ./starter.sh