package spaces

type Link struct {
	Text    string `json:"text" `
	Link    string `json:"link" `
	Comment string `json:"comment" `
	Type    string `json:"type" `
}

type More struct {
	Hint string `json:"hint" `
	Type string `json:"type" `
	Link string `json:"link" `
}

type Space struct {
	ID           string `json:"id" required:"true"`
	Space        string `json:"space" required:"true"`
	Text         string `json:"text" `
	Markdown     string `json:"markdown" `
	Links        []Link `json:"links" `
	More         More   `json:"more" `
	MarkdownHTML string
	HasEmbeds    bool
}

func New() *Space {
	s := new(Space)

	return s
}
