package spaces

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/ast"
	"github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"
)

type Markdowner struct {
	ButtonClasses  string
	MarkdownFolder string
}

func (m Markdowner) GenMarkdown(source string) template.HTML {
	s := ""
	if strings.HasPrefix(source, "http:") || strings.HasPrefix(source, "https:") {
		s = m.getMarkdownFromUrl(source)
	} else {
		s = m.getMarkdownFromFile(path.Join(m.MarkdownFolder, source))
	}

	return template.HTML(s)
}

func (m Markdowner) getMarkdownFromUrl(url string) (h string) {
	resp, err := http.Get(url)
	// handle the error if there is one
	if err != nil {
		fmt.Println(err)
		return
	}
	// do this now so it won't be forgotten
	defer resp.Body.Close()
	// reads html as a slice of bytes
	html, err := io.ReadAll(io.Reader(resp.Body))
	if err != nil {
		fmt.Println(err)
	}
	h = m.convToHtml(html)
	return
}

func (m Markdowner) getMarkdownFromFile(markdownfile string) (h string) {
	data, err := os.ReadFile(markdownfile)
	if err != nil {
		fmt.Println("File reading error", err)
		return
	}
	h = m.convToHtml(data)
	return

}

func (m Markdowner) convToHtml(data []byte) (h string) {
	extensions := parser.CommonExtensions | parser.HardLineBreak | parser.AutoHeadingIDs
	parser := parser.NewWithExtensions(extensions)
	opts := html.RendererOptions{
		Flags:          html.HrefTargetBlank,
		RenderNodeHook: m.renderHookDropCodeBlock,
	}
	renderer := html.NewRenderer(opts)
	data2 := bytes.Replace(data, []byte("\r\n"), []byte("\n"), -1)
	maybeUnsafeHTML := markdown.ToHTML(data2, parser, renderer)

	//hBytes := bluemonday.UGCPolicy().SanitizeBytes(maybeUnsafeHTML)
	h = string(maybeUnsafeHTML)
	return
}

func (m Markdowner) getMarkDownSource(markdownfile string) (s string) {
	data, err := os.ReadFile(path.Join(m.MarkdownFolder, markdownfile))
	if err != nil {
		fmt.Println("File reading error", err)
		return
	}
	s = string(data)
	s = strings.Replace(s, "\r\n", "\n", -1)

	return
}

// return (ast.GoToNext, true) to tell html renderer to skip rendering this node
// (because you've rendered it)
func (m Markdowner) renderHookDropCodeBlock(w io.Writer, node ast.Node, entering bool) (ast.WalkStatus, bool) {
	// skip all nodes that are not CodeBlock nodes
	if _, ok := node.(*ast.Link); ok {

		link := node.(*ast.Link)
		// link.Title = []byte("uli")
		link.AdditionalAttributes = append(link.AdditionalAttributes, fmt.Sprintf("class=\"%s\"", m.ButtonClasses))
		return ast.GoToNext, false
	}

	// custom rendering logic for ast.CodeBlock. By doing nothing it won't be
	// present in the output

	return ast.GoToNext, false
}

func (m Markdowner) saveMarkDownSource(filename string, data string) error {
	file, err := os.Create(path.Join(m.MarkdownFolder, filename))
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.WriteString(file, data)
	if err != nil {
		return err
	}
	return file.Sync()
}
