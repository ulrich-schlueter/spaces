package spaces

import (
	"fmt"
	"log"
	"os"
	"sort"
)

type Manager struct {
	Name         string
	Pages        map[string]*Page
	CurrentPage  *Page
	spacesFolder string
}

func NewManager(name string, spacesFolder string) *Manager {
	m := &Manager{
		Name:         name,
		Pages:        make(map[string]*Page),
		spacesFolder: spacesFolder,
	}
	m.InitPages()
	return m
}

func (m *Manager) SortedPageNames() []string {
	keys := make([]string, len(m.Pages))

	i := 0
	for k := range m.Pages {
		keys[i] = k
		i++
	}

	sort.Strings(keys)
	return keys
}

func (m *Manager) SpaceByID(pagename string, ID string) (*Space, error) {
	page, ok := m.Pages[pagename]
	if !ok {
		return nil, fmt.Errorf("Page does note exist")
	}

	for _, space := range page.Spaces {
		if space.ID == ID {
			return space, nil
		}
	}
	return nil, fmt.Errorf("Space not found in Page")
}

func (m *Manager) addPage(name string, p *Page) {
	m.Pages[name] = p
	for _, s := range p.Spaces {
		for _, l := range s.Links {
			if l.Type == "embed" {
				s.HasEmbeds = true
			}
		}

	}
}

func (m *Manager) InitPages() error {

	fileInfos, err := os.ReadDir(m.spacesFolder)
	if err != nil {
		log.Print(err.Error())
		return err
	}

	for _, file := range fileInfos {
		fmt.Println(file.Name(), file.IsDir())
		if !file.IsDir() {
			p, err := readYaml(m.spacesFolder + "/" + file.Name())
			if err != nil {
				log.Print(err.Error())
			} else {
				m.addPage(file.Name(), p)
			}

		}
	}

	return err
}
