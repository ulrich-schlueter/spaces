package spaces

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"gopkg.in/yaml.v2"
)

type Page struct {
	Name   string   `json:"name" required:"true"`
	Spaces []*Space `json:"spaces"  required:"true"`
}

func readYaml(file string) (*Page, error) {

	p := Page{}
	data, err := os.ReadFile(file)
	if err != nil {
		fmt.Println("File reading error", err)
		return nil, err
	}

	err = yaml.Unmarshal([]byte(data), &p)
	if err != nil {
		log.Printf("yaml error: %v", err)
		return nil, err
	}

	for i := range p.Spaces {
		p.Spaces[i].ID = strconv.Itoa(i)
	}

	return &p, nil

}
