package config

type Config struct {
	ButtonClasses  string
	MarkdownFolder string
	SpacesFolder   string
	Htmlfolder     string
	ViewFolder     string
}

func Default() Config {
	config := Config{
		ButtonClasses:  "btn btn-outline-primary",
		MarkdownFolder: "spaces/markdowns",
		SpacesFolder:   "spaces",
		Htmlfolder:     "html",
		ViewFolder:     "html/views",
	}

	return config
}
